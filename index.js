require('dotenv').config();
const express = require('express');

console.log('[LIFECYCLE] Application started at', new Date());

const app = require('./src/config/express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./assets/swagger');

(async () => {
    const database = await require('./src/config/database')();
    const passport = await require('./src/config/auth')(database);
    app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    app.use('/login',require('./src/endpoints/login')());
    app.use('/issues',require('./src/endpoints/issues')(database));
    app.use('/domains',require('./src/endpoints/domains')(database));
    app.use('/uploads',express.static('./uploads'));

    app.use('/$',
        require('./src/endpoints/index')());
    // app.use('/$',        passport.authenticate('jwt', { session: false }),
    //     require('./src/endpoints/index')());
    app.listen(process.env.SRV_PORT);
})();
