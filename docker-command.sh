#!/bin/sh

npm install
if [ ! -z "$ENV" ] && [ "$ENV" == "dev" ];then
    npm test
else
    npm start
fi
