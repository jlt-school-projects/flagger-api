const fixtures = require('node-mongoose-fixtures');


console.log("[LIFECYCLE] Started fixtures load", new Date());
require('dotenv').config();

(async () => {
    const mongoose = await require('../config/database')();

    console.log('[FIXTURES]','Clearing database');
    return fixtures.reset(null, mongoose, () => {
        console.log('[FIXTURES]',"Cleared database");
        fixtures({
            User: require('./User'),
            Domain: require('./Domain'),
            Issue: require('./Issue')
        },mongoose, (err, data) => {
            if(err) {
                console.error(err);
                process.exit();
            }
            console.log('[FIXTURES]','Created fixtures');
            process.exit();
        });
    });
})();
