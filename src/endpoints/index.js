const express = require('express');

module.exports = () => {
    const router = express.Router();

    router.get('/',(req,res) => {
        console.log(req.body);
        console.log(req.query);
        const response = {
            'name': 'flagger-api'
        };
        res.send(response);
    });

    router.post('/',(req,res) => {
        console.log(req.body);
        console.log(req.query);
        const response = {
            'name': 'flagger-api',
            'greetings': `Welcome ${req.body.name}`
        };
        res.send(response);
    });

    return router;
};
