const express = require('express');
const jwt = require('jsonwebtoken');
const passport = require('passport');

module.exports = () => {
    const router = express.Router();

    router.get('/',(req,res ,next) => {
            passport.authenticate('local',null,(err, user, info) => {
                console.log(info);
                if (err) { return next(err); }

                // respond with json
                if (req.accepts('application/json')) {
                    if(!user) {
                        res.status(401).send(info);
                    } else {
                        const token = jwt.sign(
                            {
                                name: `${user.firstname} ${user.lastname}`,
                                email: user.email
                            },
                            process.env.JWT_SECRET,
                            {
                                expiresIn: "1 day",
                                subject: user.email
                            }
                        );
                        res.send({
                            token
                        });
                    }
                }
            })(req, res, next);
        }
    );
    return router;
};
