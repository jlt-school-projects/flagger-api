const express = require('express');

module.exports = (database) => {
    const router = express.Router();

    router.get('/',async (req,res) => {
        const domains = await database.models.Domain.find().populate('owner','-password_hash').populate('admins','-password_hash');
        res.send(domains);
    });

    return router;
};
