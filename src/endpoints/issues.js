const express = require('express');
var multer  = require('multer')
var upload = multer({ dest: 'uploads/' });

module.exports = (database) => {
    const router = express.Router();

    router.get('/', async (req,res) => {
        console.log("GET");
        const issues = await database.models.Issue.find().populate('reporter').populate('domain');
        res.send(issues);
    });

    router.post('/',upload.array('images'), async (req,res) => {
        console.log("POST");
        console.log(req.body);
        console.log(req.files);
        try {
            req.body.issue = JSON.parse(req.body.issue);
        } catch (e) {
            res.status(400).send();
        }
        if(req.body.issue) {
            const newissue = new database.models.Issue({
                description: req.body.issue.description,
                title : req.body.issue.title,
                domain: req.body.issue.domain,
                images: req.files.map((file) => {return file.path}),
                location: req.body.issue.location,
            });

            try {
                const savedissue = await newissue.save();
                const populatedIssue = await database.models.Issue.populate(savedissue,{path: "domain"});
                res.send(populatedIssue);
            }catch (e) {
                console.error(e);
                res.status(400).send();
            }
        }



    });

    return router;
};
