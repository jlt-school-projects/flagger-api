const GeoJSON = require('mongoose-geojson-schema');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const name = 'Issue';
const IssueSchema = Schema({
   title: String,
   description: String,
   images: [String],
   reporter: { type: Schema.Types.ObjectId, ref: 'User' },
   location: mongoose.Schema.Types.Point,
   domain: { type: Schema.Types.ObjectId, ref: 'Domain' },
});

const IssueModel = mongoose.model(name,IssueSchema);
module.exports = IssueModel;
