const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const name = 'User';
const UserSchema = Schema({
   first_name: String,
   last_name: String,
   email: String,
   password_hash: String
});

const UserModel = mongoose.model(name,UserSchema);
module.exports = UserModel;
