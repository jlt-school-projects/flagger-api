const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const name = 'Domain';
const DomainSchema = Schema({
   name: String,
   owner:  { type: Schema.Types.ObjectId, ref: 'User' },
   admins: [ { type: Schema.Types.ObjectId, ref: 'User' }],
});

const DomainModel = mongoose.model(name,DomainSchema);
module.exports = DomainModel;


