const glob = require( 'glob' )
    , path = require( 'path' );


module.exports = (mongoose) => {
    const models = {};
    glob.sync( `${__dirname}/*.js`).forEach( function( file ) {
        const model = require(path.resolve(file));
        if(model.modelName) {
            models[model.modelName] = model;
        }
    });
    return models;
};

