const mongoose = require('mongoose');


const connectionString = `mongodb://${process.env.DB_HOST}/${process.env.DATABASE_NAME}?authSource=admin`;
module.exports = async () => {
    await mongoose.connect(connectionString, {
        useNewUrlParser : true,
        user: process.env.DATABASE_USERNAME,
        pass: process.env.DATABASE_PASSWORD,
        useUnifiedTopology: true
    });
    const models = require('../models/index')(mongoose);

    return mongoose;
};
