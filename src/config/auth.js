const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

function init(mongoose) {
    // JWT
    const JwtStrategyOptions = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    };
    const jwtStrategy = new JwtStrategy(JwtStrategyOptions, (payload, done) => {

        console.log(payload);
        mongoose.models.User.findOne({
        email: payload.email
        }).then(user => {
            console.log(user);
            if(user) {
                return done(null, user)
            }else {
                return done(null, false);
            }
        }).catch(e => {
            return done(e, false)
        });
    });


    // STANDARD
    const localStrategy = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },async (email, password, done) => {
        try {
            const user = await mongoose.models.User.findOne(
                { email: email  }
            );
            if(user) {
                console.log(user);
                bcrypt.compare(password,user.password_hash).then(r=> {
                    console.log(r);
                    console.log(user);
                    if(r) {
                        return done(null, user)
                    }else {
                        return done(null, false, {
                            message: 'Invalid credentials'
                        });
                    }
                });
            }else {
                return done(null, false, {
                    message: 'Unknown user'
                });
            }
        }catch (e) {
            done(e);
        }
    });
    //
    // // Save the user's email address in the cookie
    // passport.serializeUser((user, cookieBuilder) => {
    //     cookieBuilder(null, user.email);
    // });

    // function deserializeCallback(email, cb) {
    //     // Fetch the user record corresponding to the provided email address
    //     User.findOne({
    //         where : { email }
    //     }).then(r => {
    //         if(r) return cb(null, r);
    //         else return cb(new Error("No user corresponding to the cookie's email address"));
    //     });
    // }

    // passport.deserializeUser(deserializeCallback);
    //
    // passport.deserializeCallback = deserializeCallback;


    // STRATEGY MANAGEMENT
    passport.use(jwtStrategy);
    passport.use(localStrategy);

    return passport;
}

module.exports = init;
